import React, { Component } from 'react';
import '../node_modules/react-bootstrap-table/css/react-bootstrap-table.css'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Panel, Col } from 'react-bootstrap';
import _ from 'lodash';
import './AccessFormView.js';
import AccessFormView from './AccessFormView.js';
import './AccessListDetailView.css';

class AccessListDetailView extends Component {


    constructor(props) {
        super(props);
        this.onRowSelect = this.onRowSelect.bind(this);
        this.updateRow =this.updateRow.bind(this);
        this.state = {
          selectedRowObj: null,
          data: []
        };
        this.baseStae = [];
      }

   /**
    * Find the the Reader based on the readerId of the table row
    */
    getReaderRecord = (cell, row) => {
        //use the readerId  to find the Reader Record
        let readerRecord = _.find(this.props.readers, function (reader) {
            return reader.id === cell;
        })

        return readerRecord;
    }

    /**
     *  Get the Reader Type for the row  
     */
    getReaderType = (cell, row) => {
        let readerType = "";

        // Get the Reader record
        let readerRecord = this.getReaderRecord(cell, row);

        // Use the Reader Record typeId to find the Display string for the Reader Type
        if (readerRecord !== undefined) {
            readerType = _.find(this.props.readerTypes, function(type) {
                return type.id === readerRecord.typeId;
            })
        }

        return readerType.name;
    }

    /** 
     * Get Reader column data
     * 
     */
    getReaders = (cell, row) => {
        // Get the Reader record
        let readerRecord = this.getReaderRecord(cell, row);

        return (readerRecord !== undefined ? readerRecord.name : "");
    }

    /**
     * Create the data to display in the table
     */
    createTableData = () => {
        let tableData = [];
        
        tableData = this.props.accessLevels.map((record) => { 
            var newRecord = record;
            newRecord.readerType = this.getReaderType(record.readerId);
            newRecord.readers = this.getReaders(record.readerId);
            return newRecord;
         });

         return tableData;
    }

    /** 
     * Update the table row data
     */
    updateRow = (updatedRec) => {
        //update row data and force render
        let newData = this.state.data;
        let index = _.findIndex(newData, function (rec) { return rec.id === updatedRec.id});
        newData[index] = updatedRec;
        this.setState({
            data: newData
        });


    }

    //get the  details div if  row is selected
    getAccessForm =  () => {
        if (this.state.selectedRowObj) {
            return (
                <div>
                    <AccessFormView
                        accessLevelRecord={this.state.selectedRowObj}
                        callBack={this.updateRow}>
                    </AccessFormView>
                </div>

            );
        }
        return;
    }

    /**
     * Set the slected row to the state
     */
    onRowSelect(row, isSelected) {
        if (isSelected) {

          this.setState({
            selectedRowObj: row
          });
        } 
      }

      componentWillMount() {
          //if first load create tabLe data
          if (this.state.data.length === 0) {
            let tabletData = this.createTableData();
            this.setState(
                {
                    data: tabletData,
                });
          }    
      }

    render() {

        const selectRowProp = {
            mode: 'radio',
            clickToSelect: true,  // enable click to select
            onSelect: this.onRowSelect
          };

        return (
      <div className="list-detail-container">
        <Panel header="Access Levels" className="page-header">
            <Col sm={6}>
                <BootstrapTable 
                    data={this.state.data}
                    search striped={true} 
                    hover={true} 
                    selectRow={ selectRowProp } >
                    
                    <TableHeaderColumn dataField="id" hidden={true} isKey dataAlign="center" dataSort={true}></TableHeaderColumn>
                    <TableHeaderColumn dataField="name" dataAlign="center" dataSort={true}>Name</TableHeaderColumn>
                    <TableHeaderColumn dataField="readerType" dataSort={true}>Reader Type</TableHeaderColumn>
                    <TableHeaderColumn dataField="readers" dataSort={true}>>Reader(s)</TableHeaderColumn>
                </BootstrapTable>
            </Col>
            <Col sm={6}>
                {this.getAccessForm()}
            </Col>
        </Panel>
      </div>
    );
  }
}

export default AccessListDetailView;
