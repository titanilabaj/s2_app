import React, { Component } from 'react';
import AccessListDetailView from './AccessListDetailView.js';
import accessLevelsData from './accessLevels.json';
import readersData from './readers.json';
import readerTypesData from './readerTypes.json'

class App extends Component {
 
  render() {
    return (
        <div>
          <AccessListDetailView
            accessLevels={accessLevelsData}
            readers={readersData}
            readerTypes={readerTypesData}>
          </AccessListDetailView>
        </div>
    );
  }
}

export default App;
