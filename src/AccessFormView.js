import React, { Component } from 'react';
import { FormControl, Form, FormGroup, ControlLabel, Button, ButtonToolbar, Col } from 'react-bootstrap';
import readerTypes from './readerTypes.json'
import _ from 'lodash';

class AccessFormView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            accessLevelRecord: []
        };
        // keep track of base state
        this.baseState = _.clone(props.accessLevelRecord);
      }

    /** 
     * Reads in the list of Reader Types and creates the options for the
     * selection Box
     */
    getReadersList = () => {
        let readersOptions = [];

        readerTypes.forEach((reader, index) => {
            readersOptions.push(<option key={index} value={reader.name}>{reader.name}</option>)
        });

        return readersOptions;
    }

    /** 
     * Handles Changes to the Name input
     */
    handleChangeName = (event) => {
        let rec = this.state.accessLevelRecord;

        rec.name = event.target.value;
        this.setState({
            accessLevelRecord: rec
        });
    }

    /** 
     * Handles Changes to the Description input
     */
    handleChangeDescription = (event) => {
        let rec = this.state.accessLevelRecord;

        rec.Description = event.target.value;
        this.setState({
            accessLevelRecord: rec
        });
    }

    /**
     * Handles changes to the Reader type selection 
     */
    handleChangeReaderType = (event) => {
        let rec = this.state.accessLevelRecord;

        rec.readerType = event.target.value;
        this.setState({
            accessLevelRecord: rec
        });
    }

    /**
     * Call parent component function to trigger rerender
     */
    updateAccessLevel = () => {
        //call function in parent do that the whole page rerenders
        this.props.callBack(this.state.accessLevelRecord);

    }

    /**
     * reset form to the initail state
     */
    resetForm = () => {
           this.setState({
                accessLevelRecord: this.baseState
            });
    }

    submitHandler = (e) => {
        //stop enter from submitting form
        e.preventDefault();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            accessLevelRecord: nextProps.accessLevelRecord
        });
        // reset the base state to match the received props
        this.baseState = _.clone(nextProps.accessLevelRecord);
    }

    componentWillMount() {
        let clone = _.clone(this.props.accessLevelRecord);
        this.setState({
            accessLevelRecord: clone
        });
    }

    render() {
        return (
        <div className="access-form-container">
            <h3>Access Name</h3>
            <Form horizontal className="access-name-form" onSubmit={this.submitHandler}>
                <FormGroup controlId="nameId">
                    <Col sm={2}>
                        <ControlLabel>Name:</ControlLabel>
                    </Col>
                    <Col sm={10}>
                        <FormControl type="text" value={this.state.accessLevelRecord.name} onChange={this.handleChangeName.bind(this)}/>
                    </Col>
                </FormGroup>

                <FormGroup controlId="DescriptionId" >
                    <Col sm={2}>
                        <ControlLabel>Description:</ControlLabel>
                    </Col>
                    <Col sm={10}>
                        <FormControl componentClass="textarea" value={this.state.accessLevelRecord.Description} onChange={this.handleChangeDescription.bind(this)}/>
                    </Col>
                </FormGroup>

                <FormGroup controlId="ReaderId">
                    <Col sm={2}>
                        <ControlLabel>Reader(s):</ControlLabel>
                    </Col>
                    <Col sm={10}>
                        <FormControl componentClass="select" value={this.state.accessLevelRecord.readerType} onChange={this.handleChangeReaderType.bind(this)} >
                            {this.getReadersList()}
                        </FormControl>
                    </Col>
                </FormGroup>
                <ButtonToolbar>
                    <Button type="button" onClick={this.updateAccessLevel}>
                        Save
                    </Button><Button type="button" onClick={this.resetForm.bind(this)}>
                        Cancel
                    </Button>
                </ButtonToolbar>

            </Form>
      </div>
    );
  }
}

export default AccessFormView;
